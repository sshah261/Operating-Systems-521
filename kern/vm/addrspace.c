/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <uio.h>
#include <mips/tlb.h> 
#include <addrspace.h>
#include <vm.h>
#include <proc.h>
#include <spinlock.h>
#include <coremap.h>
#include <spl.h>
#include <synch.h>
#include <vnode.h>
#include <vfs.h>
#include <current.h>
#include <procsystemcall.h>
#include <kern/stat.h>
#include <kern/fcntl.h>

/*
 * Note! If OPT_DUMBVM is set, as is the case until you start the VM
 * assignment, this file is not compiled or linked or in any way
 * used. The cheesy hack versions in dumbvm.c are used instead.
 */

struct lock *bitmap_lock;
struct bitmap *disk_bitmap;
struct vnode *disk_vnode;
int swap_enabled = 0;
unsigned long disk_size;

/*
 * Creating an empty address space struct
 */

struct addrspace *
as_create(void)
{
	struct addrspace *as;
	as = kmalloc(sizeof(struct addrspace));
	if (as == NULL) {
		return NULL;
	}
	
	as->region = (struct addr *)kmalloc(sizeof(struct addr));
	if(as->region == NULL){
		kfree(as);
		return NULL;
	}
	as->region->region_start = 0;
	as->region->region_size = 0;
	as->region->region_permission = 0;
	as->region->next_region = NULL;

	as->pg = (struct page_table *)kmalloc(sizeof(struct page_table));
	if(as->pg == NULL){
		kfree(as->region);
		kfree(as);
		return NULL;
	}
	as->pg->vpn = 0;
	as->pg->physical_page_addr = 0;
	as->pg->page_state = 0;
	as->pg->next_page = NULL;
	as->pg->disk_index = disk_size;

	as->heap_start = 0;
	as->heap_end = 0;
	as->fork_lock = lock_create("fork_lock");

	return as;
}

/*
 * Swapin a page from disk
 */

static void swapin(unsigned long index, paddr_t pa)
{
        /*Initialising the uio struct for reading from the disk*/
	struct uio u;
        struct iovec iov;
        uio_kinit(&iov, &u, (void *)PADDR_TO_KVADDR(pa), 4096, index * 4096, UIO_READ);

        int err = VOP_READ(disk_vnode, &u);
        (void)err;

        lock_acquire(bitmap_lock);

	/*Setting the index in disk as free*/
	if(bitmap_isset(disk_bitmap, index))
        	bitmap_unmark(disk_bitmap, index);
	
        lock_release(bitmap_lock);

}

/*
 * Copying all the page table entries
 */

static struct page_table * copy_page_table(struct addrspace *parent_as, int *err, struct addrspace *as)
{
	/*Setting the 'current_parent' pointer to point to the first element in the parent's page table, which comes after the dummy root*/
	struct page_table *current_parent = parent_as->pg->next_page;
	struct page_table *previous_child = NULL;
	struct page_table *head_child = NULL;

	while(current_parent != NULL)
	{
		struct page_table *current_child = (struct page_table *)kmalloc(sizeof(struct page_table));	
		if(swap_enabled) /*All the child pages are allocated on disk*/
		{
			unsigned long page_number = (current_parent->physical_page_addr / PAGE_SIZE);
        	        struct page_stat *lock_in_coremap_parent = coremap + page_number;
                	
			/*Acquiring a lock to determine the current state of the page*/
			lock_acquire(lock_in_coremap_parent->page_lock);

			lock_in_coremap_parent->lru = 1; /*Marking the parent page as recently used*/
			current_child->vpn = current_parent->vpn; 
			current_child->page_state = DISK;
			
			/*Checking if the parent page is in memory*/
			if(current_parent->page_state == 0)
			{
				/*Making the child page to point to the same physical page as the parent*/
				current_child->physical_page_addr = current_parent->physical_page_addr;

				/*Allocating space on the disk for the child*/
				lock_acquire(bitmap_lock);
				bitmap_alloc(disk_bitmap, &current_child->disk_index);
				lock_release(bitmap_lock);
				
				/*Initialising the uio structure to write to the child's page on disk*/
				struct uio u;
				struct iovec iov;
				uio_kinit(&iov, &u, (void *)PADDR_TO_KVADDR(current_parent->physical_page_addr), 4096, current_child->disk_index * 4096, UIO_WRITE);
				int err = VOP_WRITE(disk_vnode, &u);
				(void)err;
					
				lock_release(lock_in_coremap_parent->page_lock);
			}
			else /*Parent's page is on disk*/
			{
				lock_release(lock_in_coremap_parent->page_lock);
				
				/*Allocating space in memory to swap the parent's page in and updating the parent's page table entry accordingly*/
				current_parent->physical_page_addr = alloc_userPage(1,current_parent->vpn,parent_as);	
				swapin(current_parent->disk_index, current_parent->physical_page_addr);
				current_parent->disk_index = disk_size;
				current_parent->page_state = 0;

				/*Making the child page to point to the same physical page as the parent*/
				current_child->physical_page_addr = current_parent->physical_page_addr;
                                
				/*Allocating space on the disk for the child*/      
                                lock_acquire(bitmap_lock);                                                      
                                bitmap_alloc(disk_bitmap, &current_child->disk_index);                          
                                lock_release(bitmap_lock);                                                      
                                                                              
				/*Initialising the uio structure to write to the child's page on disk*/                                  
                                struct uio u;                                                                   
                                struct iovec iov;                                                               
                                uio_kinit(&iov, &u, (void *)PADDR_TO_KVADDR(current_parent->physical_page_addr), 4096, current_child->disk_index * 4096, UIO_WRITE);
                                int err = VOP_WRITE(disk_vnode, &u);                                            
                                (void)err;

				unsigned long p_number_parent = (current_parent->physical_page_addr / PAGE_SIZE);
	                        struct page_stat *lk_in_coremap_parent = coremap + p_number_parent;
				lock_release(lk_in_coremap_parent->page_lock);
	
			}
			
		}

		else /*Swap is not enabled, which means the all the parent's and the child's pages will be in memory*/
		{
			if(current_child == NULL)
			{
				*err = ENOMEM;
				return NULL;
			}

			current_child->vpn = current_parent->vpn;

			/*Allocating a new page for the child*/
			current_child->physical_page_addr = alloc_userPage(1,current_child->vpn, as);
			if(current_child->physical_page_addr == 0)
	 		{	
				*err = ENOMEM;
				return NULL;
			}

			/*Copying the contents of the parent's page to the child's*/
			memmove((void *)PADDR_TO_KVADDR(current_child->physical_page_addr), (const void *)PADDR_TO_KVADDR(current_parent->physical_page_addr), 4096);
			current_child->page_state = 0;
		}
		
		/*Adding the child's page to its linked list*/
		if(previous_child == NULL)
		{
			current_child->next_page = previous_child;
			previous_child = current_child;
			head_child = current_child;
		}
		
		else
		{
			current_child->next_page = previous_child->next_page;	
			previous_child->next_page = current_child;
			previous_child = current_child;
		}

		current_parent = current_parent->next_page;
	}
	return head_child;
			
}

/*
 * Copying all the regions from the parent's address space to the child's
 */

static struct addr * copy_all_region(struct addr *parent, int *err)
{
	struct addr *current_parent = parent->next_region;
	struct addr *previous_child = NULL;
	struct addr *head_child = NULL;
	while(current_parent != NULL)
	{
		struct addr *current_child  = (struct addr *)kmalloc(sizeof(struct addr));
		if(current_child == NULL)
		{
			*err = ENOMEM;
			return NULL;
		}

		current_child->region_start = current_parent->region_start;
		current_child->region_size = current_parent->region_size;
		current_child->region_permission = current_parent->region_permission;
		if(previous_child == NULL)
		{
			current_child->next_region = previous_child;
			previous_child = current_child;
			head_child = current_child;
		}
		
		else
		{
			current_child->next_region = previous_child->next_region;
			previous_child->next_region = current_child;
			previous_child = current_child;
		}
			
		current_parent = current_parent->next_region;
	}
	return head_child;	
}

/*
 * Copying the parent's address space to the child's
 */

int
as_copy(struct addrspace *old, struct addrspace **ret)
{
	struct addrspace *newas;
	int err = 0;
	newas = as_create();
	if (newas==NULL) {
		return ENOMEM;
	}

	/*Copying all the page table entries*/
	newas->pg->next_page = copy_page_table(old, &err, newas);
	if(err != 0) {
		return err;
	}
	
	/*Copying all the regions of the parent to the child*/
	newas->region->next_region = copy_all_region(old->region, &err);
	if(err != 0) {
		return err;
	}

	/*Setting the child's heap start and end to reflect the same as the parent's heap*/
	newas->heap_start = old->heap_start;
	newas->heap_end = old->heap_end;

	*ret = newas;
	return 0;
}

/*
 * Deleting all the entries in the page table and freeing the corresponding memory locations
 */

void delete_page_table(struct page_table *root)
{
	struct page_table *current = root->next_page;
	struct page_table *ahead = NULL;

	while(current != NULL)
	{
		ahead = current->next_page;
		
		/*If swap is enabled, we need to check if each page lies in memory or on the disk*/
		if(swap_enabled)
                {
                        unsigned long page_number = (current->physical_page_addr / PAGE_SIZE);
                        struct page_stat *lock_on_coremap = coremap + page_number;
                        lock_acquire(lock_on_coremap->page_lock);
		
			if(current->page_state == 0) /*The page is in memory and we need to free it*/
			{
				free_userPage(current->physical_page_addr);
				lock_release(lock_on_coremap->page_lock);
			}
			
			else /*The page is in disk; we just mark the index as unused*/
			{
				lock_release(lock_on_coremap->page_lock);
				
				lock_acquire(bitmap_lock);
				bitmap_unmark(disk_bitmap, current->disk_index);
				lock_release(bitmap_lock);
			}		
                }
	
		else /*Swap isn't enabled, so each page is in memory. Free it.*/
			free_userPage(current->physical_page_addr);
		
		kfree(current);
		current = ahead;
		root->next_page = ahead;
	}
}

/*
 * Deleting the region linked list
 */

void delete_all_region(struct addr *root)
{
	struct addr *current = root->next_region;
	struct addr *ahead = NULL;

	while(current != NULL)
	{
		ahead = current->next_region;
		kfree(current);
		current = ahead;
	}
}

/*
 * Destroying the address space struct and freeing the memory
 */

void
as_destroy(struct addrspace *as)
{
	lock_destroy(as->fork_lock);
	delete_page_table(as->pg);

	/*Destroying the dummy root*/
	kfree(as->pg);

	delete_all_region(as->region);
	kfree(as->region);

	kfree(as);
}

/*
 * Invalidating all the entries in the TLB
 */

void
as_activate(void)
{
	int i, spl;
	struct addrspace *as;

	as = proc_getas();
	if (as == NULL) {
		return;
	}

	/* Disable interrupts on this CPU while frobbing the TLB. */
	spl = splhigh();

	for (i=0; i<NUM_TLB; i++) {
		tlb_write(TLBHI_INVALID(i), TLBLO_INVALID(), i);
	}

	splx(spl);
}

void
as_deactivate(void)
{
	/*
	 * Write this. For many designs it won't need to actually do
	 * anything. See proc.c for an explanation of why it (might)
	 * be needed.
	 */
}

/*
 * Adding a region to the linked list
 */

void insert_region(struct addr *head, struct addr *new_region)
{
	new_region->next_region = head->next_region;
	head->next_region = new_region;	
}

/*
 * This function is called by load_elf and it loads each region, which is then added to the linked list
 */

int
as_define_region(struct addrspace *as, vaddr_t vaddr, size_t memsize,int readable, int writeable, int executable)
{
	struct addr *new_region = (struct addr *)kmalloc(sizeof(struct addr));
	if(new_region == NULL)
		return ENOMEM;
	
	/*Defining the start page of the region*/
	new_region->region_start = (vaddr & PAGE_FRAME);
	new_region->region_size = memsize;

	/*Assigning permissions to the region*/
	readable = readable << 2;
	writeable = writeable << 1;
	new_region->region_permission = (new_region->region_permission | readable | writeable | executable);

	/*Adding the new region to the linked list*/
	insert_region(as->region, new_region);
	return 0;
}


int
as_prepare_load(struct addrspace *as)
{
	/*
	 * Write this.
	 */

	(void)as;
	return 0;
}

/*
 * Setting the start of the heap to point to the next page after the last region
 */

int
as_complete_load(struct addrspace *as)
{

	vaddr_t last_region_start = as->region->next_region->region_start;
	size_t last_region_size = as->region->next_region->region_size;
	vaddr_t last_region_end = last_region_start + last_region_size;
	vaddr_t last_region_page = last_region_end & PAGE_FRAME;
	as->heap_start = last_region_page + (4 * 1024);
	as->heap_end = as->heap_start;
	return 0;
}

int
as_define_stack(struct addrspace *as, vaddr_t *stackptr)
{

	/* Initial user-level stack pointer */
	(void)as;
	*stackptr = USERSTACK;

	return 0;
}

/*
 * Returns the number of bytes allocated in memory
 */

unsigned
long
coremap_used_bytes() {
	struct page_stat *totalUsedPages = coremap;
	unsigned long noUsedPages = 0;
	for(unsigned long i = 0; i < totalPages; i++) {
		if(totalUsedPages->state != FREE)
			noUsedPages++;
		totalUsedPages++;
	}
	return (noUsedPages * 4 * 1024);
}

/*
 * This function is called when sbrk is called with a negative value
 */

void sbrk_free(struct addrspace *as, vaddr_t heap_end)
{
	struct page_table *current = as->pg->next_page;
	struct page_table *previous = as->pg;                                                             

	/*We check if any page table entry lies outside the new heap end. If so, we delete the entry and invalidate it.*/
        while(current != NULL)                                                                                  
        {                                                                                                       
		if(current->vpn > heap_end && current->vpn < (STACK_BOUNDARY)) /*The page lies beyond heap end and is not in the stack*/
		{
			if(swap_enabled) /*If swap is enabled, the page could be on disk*/
	 		{
		 		unsigned long page_number = (current->physical_page_addr / PAGE_SIZE);
			 	struct page_stat *lock_on_coremap = coremap + page_number;
				lock_acquire(lock_on_coremap->page_lock);
				
				if(current->page_state == 0) /*Page is in memory. Free it.*/
				{
					free_userPage(current->physical_page_addr);
					lock_release(lock_on_coremap->page_lock);
				}

				else /*Page is in disk. Mark the index as unused.*/
				{
					lock_release(lock_on_coremap->page_lock);
	
					lock_acquire(bitmap_lock);
					bitmap_unmark(disk_bitmap, current->disk_index);
					lock_release(bitmap_lock);
				}
			}
			
			else /*Page is always in memory*/
				free_userPage(current->physical_page_addr);

			previous->next_page = current->next_page;
			kfree(current);
			current = previous->next_page;
		}
		else /*Page is valid*/
		{
			current = current->next_page;
			previous = previous->next_page;
		}
        }
}

/*
 * If the swapped out page mapping is in the TLB of our own CPU, we invalidate it
 */

static void shootdown_own_cpu(vaddr_t virtual_addr)
{
	int spl = splhigh();  
	int index = tlb_probe((uint32_t)virtual_addr, 0);
        if(index >= 0)
                tlb_write(TLBHI_INVALID(index), TLBLO_INVALID(), index);
	
	splx(spl);
}

/*
 * Updating the page table of the owner process to indicate that the page is on disk
 */

static void update_owner_PTE(struct addrspace *as, vaddr_t faultaddress, unsigned int index)
{
	struct page_table *current = as->pg->next_page;

        while(current != NULL){                                                                                                       
                if(current->vpn == faultaddress)
		{
			current->page_state = 1;
			current->disk_index = index;
			break;
		}
                current = current->next_page;                                 
        }  
}

/*
 * Finding a page to swap out using clock LRU
 */
static void find_swap_page(struct addrspace *as, struct page_stat **search) {
	(void)as;
	
	/*Checking whether the current page is not a kernel page or has not been recently used*/
	while(rr_coremap->state == FIXED || rr_coremap->lru == 1)
	{
		/*Checking if the current page is the last page*/
		if((rr_coremap->physical_addr / 4096) == (totalPages - 1))
		{
			/*If the page is not a kernel page, we mark it as ready for swapping out*/
			if(rr_coremap->state != FIXED)
                       		 rr_coremap->lru = 0;

			/*We reached the end of the coremap, hence pointing it back to the start of free pages*/
			rr_coremap = coremap_freepage;
			continue;
		}	

		/*If the page is not a kernel page, we mark it as ready for swapping out*/
		if(rr_coremap->state != FIXED)
			rr_coremap->lru = 0;
		
		rr_coremap++;
	}
	
	/*We have found a page to swap out. Mark that page as recently used*/
	*search = rr_coremap;
	rr_coremap->lru = 1;
	
	/*Checking if the current page is the last page*/
	if((rr_coremap->physical_addr / 4096) == (totalPages - 1))
        	rr_coremap = coremap_freepage;
	else
		rr_coremap++;

}

/*
 * Swapping a page out to disk
 */

static void swapout(vaddr_t faultaddress, struct addrspace *as, struct page_stat *swap_page, struct cpu *prev_cpu)
{
	struct uio u;
	struct iovec iov;

	/*Acquiring a lock on the disk to allocate space for the page being swapped out*/
	lock_acquire(bitmap_lock);
	
	unsigned index = disk_size + 1;

	/*Finding an available index in the disk*/
	bitmap_alloc(disk_bitmap, &index);
	
	lock_release(bitmap_lock);
	
	/*Updating the page table of the owner process to indicate that the page is on disk*/
	if(as != NULL)
		update_owner_PTE(as,faultaddress,index);

	/*If the swapped out page mapping is in the TLB of our own CPU, we invalidate it*/	
	shootdown_own_cpu(faultaddress);
	(void)prev_cpu;

	/*Initialising the uio structure and writing the data onto the disk*/
	uio_kinit(&iov, &u, (void *)PADDR_TO_KVADDR(swap_page->physical_addr), 4096, index * 4096, UIO_WRITE);
	int err = VOP_WRITE(disk_vnode, &u);
	if(err != 0)
		kprintf("Data written to disk at %u & err = %d\n",index * 4096, err);
	
	/*Zeroing the location after copying the data to disk*/
	bzero((void *)PADDR_TO_KVADDR(swap_page->physical_addr),4096);

}

/*
 * Find a free page in memory and allocate it
 * Swap out a page if memory is full
 */

paddr_t
getPPages(unsigned long nPages,int flag, vaddr_t v_address, struct addrspace *as) {
	struct page_stat *search = coremap_freepage;
	unsigned long freePages = 0;
	int coremap_loop = 1;
	
	/*We start looping from the first free page until the end of memory*/
	for(unsigned long i = noKernelPages + noCoremapPages ; i < totalPages && coremap_loop == 1; i++) {
		if(search->state == FREE) {
			freePages++;
			if(freePages == nPages) { /*We have found the required number of free pages*/
				for(unsigned long j = 0; j < nPages; j++) { /*Looping back to assign the values for each entry in the coremap*/
					search->state = flag;
					search->lru = 1; /*Marking this page as recently used*/

					if(flag == FIXED) /*Since this is a kernel page, we use direct mapping to convert it to a virtual address*/
						search->virtual_addr = PADDR_TO_KVADDR(search->physical_addr);
					else if(flag == USED) {
						search->virtual_addr = v_address;
						search->proc_as = as;
						search->proc_cpu = curcpu;
					}

					if(j == nPages - 1)
						search->chunk_size = nPages; /*Assign the chunk_size to the first page of the chunk*/
					else
						search->chunk_size = 0;

					/*Zeroing out the memory before returning it to the user*/
					bzero((void *)PADDR_TO_KVADDR(search->physical_addr),4096);
					search--;
				}
				search++;

				if(swap_enabled) /*If swap is enabled, we acquire a lock on the page to prevent other processes from swapping it out*/
					    lock_acquire(search->page_lock);
				spinlock_release(&coremap_spinlock);
				return search->physical_addr;
			}
		} else
			freePages = 0;
		search++;
	}

	/*At this point, the memory is full and hence a page has to be swapped out*/
	if(swap_enabled) {
		/*Finding a page to swap out*/
		find_swap_page(as,&search);
		
		/*Since we have found a page to swap out, we can release the spinlock on the coremap*/
		spinlock_release(&coremap_spinlock);
		
		/*Acquiring a lock on the page to prevent other processes from swapping it out*/
		lock_acquire(search->page_lock);

		/*Copy the owner process metadata*/
		struct addrspace *prev_as = search->proc_as;
                vaddr_t prev_vaddr = search->virtual_addr;
                struct cpu *prev_cpu = search->proc_cpu;
		int prev_state = search->state;

		/*Changing the values of the page to reflect the current process metadata*/
                if(flag == FIXED)
                        search->virtual_addr = PADDR_TO_KVADDR(search->physical_addr);
                else if(flag == USED)
                        search->virtual_addr = v_address;

		search->state = flag; 
		search->proc_as = as;
                search->proc_cpu = curcpu;
                search->chunk_size = nPages;
		
		/*After acquring the lock, it is possible that the owner process has freed this page, so there is no need for swap out*/
		if(prev_state == FREE)
			return search->physical_addr;
		
		/*Swapping the page out to disk*/
		swapout(prev_vaddr, prev_as, search, prev_cpu);
		return search->physical_addr;
	}
	else
	{
		spinlock_release(&coremap_spinlock);
		return 0;
	}
}

/*
 * This allocates a kernel page in memory and returns its virtual address
 */

vaddr_t
alloc_kpages(unsigned nPages) {
	paddr_t pa;
	spinlock_acquire(&coremap_spinlock);
	pa = getPPages(nPages,FIXED,0, NULL);
	//spinlock_release(&coremap_spinlock);
	if(pa == 0)
		return 0;

	if(swap_enabled)
	{
		unsigned long page_number = (pa / PAGE_SIZE);
                struct page_stat *lock_in_coremap = coremap + page_number;
                lock_release(lock_in_coremap->page_lock);
	}
	
	return PADDR_TO_KVADDR(pa);
}

/*
 * This allocates a user page in memory and returns its physical address
 */

paddr_t
alloc_userPage(unsigned nPages, vaddr_t faultaddress, struct addrspace *as) {
	paddr_t pa;
	spinlock_acquire(&coremap_spinlock);
	pa = getPPages(nPages,USED,faultaddress, as);
	//spinlock_release(&coremap_spinlock);
	
	if(pa == 0)
		return 0;
	
	return pa;
}

/*
 * Freeing kernel pages
 */

void
free_kpages(vaddr_t addr) {
	paddr_t pa = addr - MIPS_KSEG0;
	unsigned long pageNumber = pa/(4*1024);

	spinlock_acquire(&coremap_spinlock);
	struct page_stat *search = coremap + pageNumber;
	unsigned long chunk_size = search->chunk_size;
	search->chunk_size = 0;
	while(chunk_size > 0) {
		search->state = FREE;
		search->virtual_addr = 0;
		search->proc_as = NULL;
		search->proc_cpu = NULL;
		search++;
		chunk_size--;
	}

	spinlock_release(&coremap_spinlock);
}

/*
 * Free a user page
 */

void                                                                                                            
free_userPage(paddr_t pa) {        
        unsigned long pageNumber = pa/(4*1024);
        spinlock_acquire(&coremap_spinlock);
                                                                    
        struct page_stat *search = coremap + pageNumber;
         
	/*If the page mapping is in the TLB, we invalidate it*/
	int index = tlb_probe((uint32_t)search->virtual_addr, 0);
	if(index >= 0)                                                         
         	tlb_write(TLBHI_INVALID(index), TLBLO_INVALID(), index);
        
	/*Marking the coremap entry as FREE and changing other parameters*/
	search->state = FREE;       
	search->proc_as = NULL;
	search->lru = 0;

	/*It may have happened that another process might be sleeping waiting for this page. Zero out the memory before the process acquires the lock.*/
	if(swap_enabled)
		bzero((void *)PADDR_TO_KVADDR(pa),4096);

        spinlock_release(&coremap_spinlock);	
}

/*
 * Looping through the region linked list to check if the faultaddress lies within the region
 */

int search_region(struct addrspace *as, struct addr *region, vaddr_t faultaddress)
{
	/*Pointing the 'current' pointer to the first element in the linked list, which is after the dummy root*/
	struct addr *current = region->next_region;
	while(current != NULL)
	{
		if(current->region_start <= faultaddress && (current->region_start + current->region_size) > faultaddress)
			return 1;
		
		current = current->next_region;	
	}	

	/*Checking whether it lies within the stack*/
	vaddr_t stack_start = STACK_END;
	size_t stack_size = PAGE_SIZE * NO_STACK_PAGES;	
	if(faultaddress < stack_start && faultaddress >= (stack_start - stack_size))
		return 1;

	/*Checking if it lies in the heap*/
	if(as->heap_start <= faultaddress && as->heap_end > faultaddress)
		return 1;

	/*At this point, it doesn't lie in any region; return invalid address*/
	return 0;
}

/*
 * Adding the element to the linked list of pages
 */

void insert_page(struct page_table *head, struct page_table *new_page)
{
	new_page->next_page = head->next_page;                                                            
        head->next_page = new_page; 
}

/*
 * Looping through the page table linked list to check if a page has been allocated for an address
 */

paddr_t search_page(struct page_table *pg, vaddr_t faultaddress, struct addrspace *as)
{
	/*Pointing the 'current' pointer to the first element in the linked list, which is after the dummy root*/
	struct page_table *current = pg->next_page;
	while(current != NULL)
	{
		if(current->vpn == faultaddress) /*This means that the page has been allocated memory and resides in the disk or in memory*/
		{
			/*Getting the index on the coremap*/
			unsigned long pageNumber = (current->physical_page_addr / PAGE_SIZE);
			struct page_stat *page_in_coremap = coremap + pageNumber;
			
			if(swap_enabled)
			{
				/*In order to prevent multiple processes from accessing the page at the same time, we acquire a lock on it*/
				lock_acquire(page_in_coremap->page_lock);

				/*We set the lru bit, marking the page as recently used*/
				page_in_coremap->lru = 1;
			}
			
			if(current->page_state == 0) /*This means that the page is in memory*/
			{
				if(swap_enabled) 
					lock_release(page_in_coremap->page_lock);

				/*Since the page is in memory, we just return its physical address*/
				return current->physical_page_addr;
			}
		
			else /*The page is on disk*/
			{	
				/*Since the pag is on the disk, the lock we acquired is not being used for this page, so release it*/
				lock_release(page_in_coremap->page_lock);

				/*Allocate a new page in memory*/
				paddr_t pa = alloc_userPage(1,faultaddress,as);
				unsigned long pNumber = (pa / PAGE_SIZE);
				struct page_stat *p_in_coremap = coremap + pNumber;
			
				/*Now we need to swapin the required page at the newly allocated memory*/	
				swapin(current->disk_index, pa);
				
				/*Update the page table entry*/
				current->page_state = 0;
			    	current->disk_index = disk_size;
				current->physical_page_addr = pa;
				
				lock_release(p_in_coremap->page_lock);
				return pa;	
			}
		}

		current = current->next_page;
	}

	/*At this point, the page has not been allocated, which means we need to allocate one for it*/
	struct page_table *new_page = (struct page_table *)kmalloc(sizeof(struct page_table));
	new_page->vpn = faultaddress;
	new_page->physical_page_addr = (alloc_userPage(1,faultaddress,as));
	new_page->page_state = 0;
	new_page->disk_index = disk_size;
	insert_page(pg, new_page);
	if(swap_enabled)
	{
		unsigned long page_number = (new_page->physical_page_addr / PAGE_SIZE);        
		struct page_stat *lock_in_coremap = coremap + page_number;
		lock_release(lock_in_coremap->page_lock);
	}
	return new_page->physical_page_addr;
}

/*
 * We use an on-demand allocation technique.
 * This means that a page is allocated to a process only when there is a vm_fault on it (when it is required)
 */

int
vm_fault(int faulttype, vaddr_t faultaddress) {
	struct addrspace *as;
	uint32_t ehi, elo;
	int err = 0;
	as = proc_getas();

	/*Making the fault address page-aligned*/
	faultaddress &= PAGE_FRAME;

	/*Checking whether the faultaddress lies within a valid region*/
	err = search_region(as, as->region, faultaddress);
	if(err == 0)
		return EFAULT;
	
	switch (faulttype) {
            case VM_FAULT_READONLY:
                /* We always create pages read-write, so we can't get this */
                panic("our vm: got VM_FAULT_READONLY\n");
            case VM_FAULT_READ:
            case VM_FAULT_WRITE:
                break;
            default:
                return EINVAL;
        }

	/*Now that the address is valid, we check whether the page has been allocated memory or if we need to allocate a new one*/	
	paddr_t physical_address  = search_page(as->pg, faultaddress, as);
	
  	if(physical_address == 0)
		return ENOMEM;

	/*Adding the mapping to the TLB in a round-robin fashion*/
	int spl = splhigh();
	if(curcpu->tlb_index == NUM_TLB)
		curcpu->tlb_index = 0;

	ehi = faultaddress; 
        elo = physical_address | TLBLO_DIRTY | TLBLO_VALID;
	tlb_write(ehi, elo, curcpu->tlb_index);
	curcpu->tlb_index += 1;
	splx(spl);
	return 0;
}

/*
 * Initialising the Virtual Memory subsystem
 */

void
vm_bootstrap(void) {

	/*Creating a global lock for the bitmap*/
	bitmap_lock = lock_create("bitmap_lock");

	/*stat structure to get the disk size*/
	struct stat buf;

	/*Opening the disk as a file*/
	const char *filename = "lhd0raw:";
	char *fname = kstrdup(filename);
	int err = vfs_open(fname, O_RDWR, O_RDWR, &disk_vnode);
	
	/*Checking whether the disk is enabled*/
	if(err)
		swap_enabled = 0;
	else {
		swap_enabled = 1;
		err = VOP_STAT(disk_vnode, &buf);
		disk_size = buf.st_size / (4*1024);
	}
	
	if(swap_enabled)
	{
		/*If swap is enabled, we create a sleep lock for each page in the coremap*/
		struct page_stat *page_spinlock = coremap;
		swap_enabled = 0;
		for(unsigned long i = 0; i < totalPages; i++) {
			page_spinlock->page_lock = lock_create("page_lock");
			page_spinlock++;
		}
		swap_enabled = 1;
	
		disk_bitmap = bitmap_create(disk_size);
	}

	kfree(fname);
}

void
vm_tlbshootdown(const struct tlbshootdown *ts) {
        (void)ts;
	return;
}
