#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <uio.h>
#include <proc.h>
#include <current.h>
#include <addrspace.h>
#include <vnode.h>
#include <elf.h>
#include <kern/unistd.h>
#include <vfs.h>
#include <copyinout.h>
#include <proc.h>
#include <kern/stat.h>
#include <spinlock.h>
#include <filesystemcall.h>
#include <lib.h>

ssize_t read(int fd, const void *buf, size_t buflen, int *flag)
{
	/*File descriptor should be valid*/
	if(fd < 0 || fd > 63)
	{
		*flag = EBADF;
		return 0;
	}

	/*Index in file table should be valid*/
	if(curproc->p_filetable[fd] == NULL)
	{
		*flag = EBADF;
		return 0;
	}	

	/*File should not have been in O_WRONLY mode*/
	if((curproc->p_filetable[fd]->fhandle_flag & 11) == 1) 
	{
		*flag = EBADF;
		return 0;
	}
	
	lock_acquire(curproc->p_filetable[fd]->fhandle_lock);
	struct iovec iov;
	struct uio u;
	iov.iov_ubase = (userptr_t)buf;
	iov.iov_len = buflen;
	u.uio_iov = &iov;
	u.uio_iovcnt = 1;
	u.uio_resid = buflen;
	u.uio_offset = curproc->p_filetable[fd]->fhandle_offset;
	u.uio_segflg = UIO_USERSPACE;
	u.uio_rw = UIO_READ;
	u.uio_space = proc_getas();
	int err = VOP_READ(curproc->p_filetable[fd]->fhandle_vnode,&u);
	if(err != 0)
	{
		*flag = err;
		lock_release(curproc->p_filetable[fd]->fhandle_lock);
		return 0;
	}

	/*Set the new offset after reading*/
	curproc->p_filetable[fd]->fhandle_offset = curproc->p_filetable[fd]->fhandle_offset + buflen - u.uio_resid;
	lock_release(curproc->p_filetable[fd]->fhandle_lock);
	return (buflen - u.uio_resid);

}

ssize_t write(int fd, const void *buf, size_t buflen, int *flag)
{
 	/*Making sure that the given file descriptors are valid*/
	if(fd < 0 || fd > 63)
	{
		*flag = EBADF;
		return 0;
	}

	/*Making sure the index in the file table is valid*/
	if(curproc->p_filetable[fd] == NULL)
	{
		*flag = EBADF;
		return 0;
	}
	
	/*Making sure that the file has not been opened in O_RDONLY mode*/
	if((curproc->p_filetable[fd]->fhandle_flag & 11) == 0)
	{
		*flag = EBADF;
		return 0;
	}

	/*Making sure that the buffer to read from is not null*/
	if(buf == NULL)
	{
		*flag = EFAULT;
		return 0;
	}

	lock_acquire(curproc->p_filetable[fd]->fhandle_lock);
	struct iovec iov;
	struct uio u;
	iov.iov_ubase = (userptr_t)buf;
	iov.iov_len = buflen;
	u.uio_iov = &iov;
	u.uio_iovcnt = 1;
	u.uio_resid = buflen;
	u.uio_offset = curproc->p_filetable[fd]->fhandle_offset;
	u.uio_segflg = UIO_USERSPACE;
	u.uio_rw = UIO_WRITE;
	u.uio_space = proc_getas();
	int err = VOP_WRITE(curproc->p_filetable[fd]->fhandle_vnode,&u);
	if(err != 0)
	{
		*flag = err;
		lock_release(curproc->p_filetable[fd]->fhandle_lock);
		return 0;
	}

	/*Setting the new offset of the file*/
	curproc->p_filetable[fd]->fhandle_offset = curproc->p_filetable[fd]->fhandle_offset + buflen - u.uio_resid; 
	lock_release(curproc->p_filetable[fd]->fhandle_lock);

	return (buflen - u.uio_resid);

}

int open(const char *filename, int flags, int *err)
{
	char f[128];
	
	/*Checking whether the file name is valid or not*/
	*err = copyinstr((userptr_t)filename, f, 128, NULL);
        if(*err != 0) {
                return 0;
        }

	/*Creating a new file handle*/
	struct file_handle *fhandle_open = fhandle_create(flags);
	*err = vfs_open(f, flags, 0, &fhandle_open->fhandle_vnode);
	if (*err != 0) {
 		return 0;
	}

	lock_acquire(fhandle_open->fhandle_lock);
	
	/*Finding an empty location in the file table*/
	int i = 0;
	for(i = 0; i < 64; i++)
	{
		if(curproc->p_filetable[i] == NULL)
		{
		  curproc->p_filetable[i] = fhandle_open;
		  break;
		}	
	}

	lock_release(fhandle_open->fhandle_lock);
	return i;
}

int close(int fd, int *flag)
{
	/*Check if the file descriptor is valid*/
	if((fd < 0) || (fd > 63))
	{
		*flag = EBADF;
		return 0;
	}
	
	/*Check whether the index in the file table is valid*/
	if(curproc->p_filetable[fd] == NULL)
	{
		*flag = EBADF;
		return 0;
	}
	
	lock_acquire(curproc->p_filetable[fd]->fhandle_lock);

	/*
	 * Checking if any other processes are using the file
	 * If there are, we simply decrement the refcount, set the file table index to point to null for curproc, and return
	 */
	if(curproc->p_filetable[fd]->fhandle_refcount > 1) {
		curproc->p_filetable[fd]->fhandle_refcount--;
		lock_release(curproc->p_filetable[fd]->fhandle_lock);
		curproc->p_filetable[fd] = NULL;
		return 0;
	}
	
	/*
	 * This is the only process using this file
	 * Safe to clear and destroy
	 * EXTERMINATE!
	 */

	vfs_close(curproc->p_filetable[fd]->fhandle_vnode);
	lock_release(curproc->p_filetable[fd]->fhandle_lock);
	lock_destroy(curproc->p_filetable[fd]->fhandle_lock);
	kfree(curproc->p_filetable[fd]);
	curproc->p_filetable[fd] = NULL;
	return 0;
}

off_t lseek(int fd, off_t pos, int whence, int *err) {

	/*File descriptor should be valid*/
	if((fd < 0) || (fd > 63)) {
		*err = EBADF; 
		return 0;
	}
	
	/*Index in file table should be valid*/
	if(curproc->p_filetable[fd] == NULL) {
		*err = EBADF; 
		return 0;
	}
	
	/*Vnode should be seekable ie, should be a valid file and not a directory or a link*/
	if(!VOP_ISSEEKABLE(curproc->p_filetable[fd]->fhandle_vnode)) {
		*err = ESPIPE; 
		return 0;
	}

	/*Whence should be an integer between 0 and 2*/
	if(whence < 0 || whence > 2 ) {
		*err = EINVAL;
		return 0;
	}

	/*Getting the file stats*/
	struct stat buf;
        
	*err = VOP_STAT(curproc->p_filetable[fd]->fhandle_vnode, &buf);
        if (*err != 0) {
		return 0;
	}

	if (whence == 0) { /*Offset should be set to pos*/
		if(pos < 0 ) { /*Resulting offset would be negative if it enters the if statement*/
			*err = EINVAL;
			return 0;
		}
			
		if(pos > buf.st_size) { /*pos is greater than file size. Increase the file size to match.*/
			buf.st_size = pos;
		}

		lock_acquire(curproc->p_filetable[fd]->fhandle_lock);
		curproc->p_filetable[fd]->fhandle_offset = pos;
		lock_release(curproc->p_filetable[fd]->fhandle_lock);
		return pos;
	}
	
	if (whence == 1) { /*Offset should be (current offset + whence)*/
		if(curproc->p_filetable[fd]->fhandle_offset + pos > buf.st_size ) /*Resulting offset would be larger than the file size. Increase the same.*/
			buf.st_size = curproc->p_filetable[fd]->fhandle_offset + pos;

		lock_acquire(curproc->p_filetable[fd]->fhandle_lock);
		curproc->p_filetable[fd]->fhandle_offset = curproc->p_filetable[fd]->fhandle_offset + pos;
		lock_release(curproc->p_filetable[fd]->fhandle_lock);
		return curproc->p_filetable[fd]->fhandle_offset;
	}
	
	/*Whence is 2. Offset should be EOF + pos*/
	if (pos < 0) { /*Offset would be less than file size. Just add and return offset.*/
		lock_acquire(curproc->p_filetable[fd]->fhandle_lock);
		curproc->p_filetable[fd]->fhandle_offset = buf.st_size +  pos;
		lock_release(curproc->p_filetable[fd]->fhandle_lock);
		return curproc->p_filetable[fd]->fhandle_offset;
	}

	/*Offset would be greater than file size. Increase and set offset.*/
	buf.st_size = buf.st_size + pos;
	lock_acquire(curproc->p_filetable[fd]->fhandle_lock);
	curproc->p_filetable[fd]->fhandle_offset = buf.st_size;
	lock_release(curproc->p_filetable[fd]->fhandle_lock);
        
	return curproc->p_filetable[fd]->fhandle_offset;
}

int chdir(const char *pathname)
{ 
	/*Checking if the passed pathname is valid*/
	char path[70];
	int err = copyin((userptr_t)pathname, path, sizeof(const char*));
	if(err != 0) {
		return -1*err;
	}

	/*Changing the current directory*/
	spinlock_acquire(&curproc->p_lock);
	err = vfs_chdir(path);
	spinlock_release(&curproc->p_lock);
	if (err != 0) {
		return -1*err;
	}

	return 0;
}

int dup2(int oldfd, int newfd)
{
	/*Checking if both file descriptors are valid*/
	if(oldfd < 0 || oldfd > 63 || newfd < 0 || newfd > 63)
		return -1*EBADF;
	
	/*If both are the same, it is a valid case which doesn't change anything in the file table. Just return success.*/
	if(newfd == oldfd)
		return newfd;

	/*Checking if oldfd is empty*/
	if(curproc->p_filetable[oldfd] == NULL)
		return -1*EBADF;

	lock_acquire(curproc->p_filetable[oldfd]->fhandle_lock);
	
	/*Checking if there is a file present at newfd and closing it if there is*/
	
	if(curproc->p_filetable[newfd] != NULL) {
		int flag = 0;
		int err = close(newfd, &flag);
		if(flag != 0) { /*Close returned an error*/
			lock_release(curproc->p_filetable[oldfd]->fhandle_lock);
			err = flag;
			return -1*err;
		}
	}

	/*At this point, newfd points to a null value*/
	curproc->p_filetable[newfd] = curproc->p_filetable[oldfd];
	curproc->p_filetable[newfd]->fhandle_refcount += 1; /*Incrementing the refcount*/
	lock_release(curproc->p_filetable[oldfd]->fhandle_lock); 
	return newfd;
}
