#include <types.h>
#include <kern/errno.h>                                                                                         
#include <lib.h>                                                                                                
#include <uio.h>                                                                                                
#include <proc.h>                                                                                               
#include <current.h>                                                                                            
#include <addrspace.h>                                                                                          
#include <vnode.h>                                                                                              
#include <elf.h>                                                                                                
#include <kern/unistd.h>                                                                                        
#include <vfs.h>                                                                                                
#include <copyinout.h>                                                                                          
#include <proc.h>                                                                                               
#include <kern/stat.h>                                                                                          
#include <spinlock.h>                                                                                           
#include <memorysystemcall.h>
#include <lib.h>

void * sbrk(intptr_t amount, int *err)
{
	/*Checking whether the amount passed is page aligned*/
	if(amount % (PAGE_SIZE) != 0)
	{
		*err = EINVAL;
		return (void *)0;
	}
	
	/*Storing the previous value of heap end as we need to return this later*/
	vaddr_t previous_val = curproc->p_addrspace->heap_end;
	
	long long int previous_dup_val = curproc->p_addrspace->heap_end;
	long long int heap_start_dup_val = curproc->p_addrspace->heap_start;

	/*If amount is negative and new heap end falls beneath heap start, return error*/
	if((amount < 0) && ((previous_dup_val + amount) < heap_start_dup_val))
	{
		*err =  EINVAL;
		return (void *)0;
	}

	vaddr_t stackend = (STACK_BOUNDARY);

	/*Deleting the pages that lie beyond the new heap end*/
	if(amount < 0)
		sbrk_free(curproc->p_addrspace, previous_val + amount);
	else if((previous_val + amount) > stackend) /*New heap end enters the stack; return error*/
	{
		*err = ENOMEM;
		return (void *)0;
	}

	/*Updating the heap end value*/
	curproc->p_addrspace->heap_end = previous_val + amount;
	
	return (void *)previous_val; 	
	
}
