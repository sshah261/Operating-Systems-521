//This file contains definitions of all the process system calls

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <uio.h>
#include <proc.h>
#include <current.h>
#include <addrspace.h>
#include <vnode.h>
#include <elf.h>
#include <kern/unistd.h>
#include <vfs.h>
#include <copyinout.h>
#include <proc.h>
#include <kern/stat.h>
#include <spinlock.h>
#include <synch.h>
#include <thread.h>
#include <kern/wait.h>
#include <kern/fcntl.h>
#include <procsystemcall.h>
#include <proctable.h>
#include <execvbuf.h>
#include <syscall.h>
#include <limits.h>
#include <kern/limits.h>

char execbuf[65536];
char* arg_pointers[3852];
//struct lock *execbuf_lock;
size_t execbuf_index = 0;
size_t number_args = 0;

void exit(int exitcode) {
	lock_acquire(curproc->proc_sleep_lock);

	/*Setting the value of exitcode and exitstatus*/
	curproc->p_exitcode = exitcode;
	curproc->p_exitstatus = _MKWAIT_EXIT(exitcode);

	/*Signal the parent saying that the child has exited*/
	cv_signal(curproc->p_cv, curproc->proc_sleep_lock);

	lock_release(curproc->proc_sleep_lock);
	thread_exit();
}

pid_t waitpid(pid_t pid, int *status, int options, int *flag) { 
	
	/*We do not support options currently, so it should be zero*/
	if(options != 0)
	{
		*flag = EINVAL;
		return 0;
	}

	/*Checking if the passed pid is valid*/
	if(pid <= 0 || pid > __PID_MAX)
	{
		*flag = ESRCH;
		return 0;
	}

	/*Checking if the passed pid is the current process's pid*/
	if(curproc->p_pid == pid)
	{
		*flag = ECHILD;
		return 0;
	}

	/*Checking if the index in the proc table is valid*/
	if(proc_table[pid] == NULL) {
		*flag = ESRCH;
		return 0;
	}

	/*Checking if the passed pid is a child of the current process*/
	if(proc_table[pid]->p_ppid != curproc->p_pid)
	{	
		*flag = ECHILD;
		return 0;
	}
	
	lock_acquire(proc_table[pid]->proc_sleep_lock);
	/*Waiting for the child to signal before it exits*/
	while(proc_table[pid]->p_exitcode == -1)
		cv_wait(proc_table[pid]->p_cv, proc_table[pid]->proc_sleep_lock);
	
	/*If status is not null, exit status of the child should be set in status*/
	if(status != NULL) {
		int err = copyout(&proc_table[pid]->p_exitstatus, (userptr_t)status, sizeof(int));
		if(err != 0 ) {
			*flag = err;
			lock_release(proc_table[pid]->proc_sleep_lock);
			proc_destroy(proc_table[pid]);
			proc_table[pid] = NULL;
			return 0;
		}
	}
	lock_release(proc_table[pid]->proc_sleep_lock);

	/*Destroying the child's proc structure*/
	proc_destroy(proc_table[pid]);

	/*Emptying the child's index in the proc table*/
	proc_table[pid] = NULL;
	return pid;
}

pid_t getpid() {
	/*Return current process's pid*/
	return curproc->p_pid;
}

int execv(char *progname, char **args,int *err) {
	struct addrspace *as;
        struct vnode *v;
        vaddr_t entrypoint, stackptr;
        int result;
	size_t gotlen = 0;
	size_t i = 0;
	char prog = '0' ;
	char arg = '0';
	
	/*Checking if the passed args is null*/
	if(args == NULL)
	{
		*err = EFAULT;
		return 0;
	}

	/*Checking if the passed progname is null*/
	if(progname == NULL)
	{
		*err = EFAULT;
		return 0;
	}

	
	/*Checking if progname is a valid address*/
	*err = copyinstr((userptr_t)progname,&prog,1000,NULL);
	if(*err != 0)
	{
		return 0;
	}

        /* Open the file. */
        result = vfs_open(progname, O_RDONLY, 0, &v);
        if (result) {
		*err = result;
                return 0;
        }

	lock_acquire(execbuf_lock);
	
	/*Cleaning the buffer of its previous contents*/
	bzero(&execbuf, sizeof(execbuf));
	bzero(&arg_pointers,sizeof(arg_pointers));
        number_args = 0;
        execbuf_index = 0;

	/*Checking whether args is a valid address*/
	*err = copyinstr((userptr_t)args, &arg, 65536, NULL);   
	if(*err != 0)
	{
		lock_release(execbuf_lock);
		return 0;
	}

	/*Copying the args into the buffer one by one*/
	i = 0;
	while(args[i] != NULL) {
			*err = copyinstr((userptr_t)args[i++], &execbuf[execbuf_index], 65536, &gotlen);
			if(*err != 0) {
				lock_release(execbuf_lock);
				return 0;
			}

			/*Aligning the read argument by 4*/
			while((gotlen % 4) != 0) { 
				//Like above, aligning the args value read by 4
				execbuf[execbuf_index + gotlen] = '\0';
				gotlen++;
			}
			execbuf_index = execbuf_index + gotlen;
			execbuf[execbuf_index -1] = '|';
			number_args++;
	}

	/*NULL pointer for the end of args*/
	execbuf[execbuf_index-1] = '\0';

	if(number_args > ARG_MAX)
	{
		*err = E2BIG;
		lock_release(execbuf_lock);
		return 0;
	}

        /*At this point, progname and args are in execbuf. Now we need to store it onto the user stack. This will be done after the user stack is defined.*/

	/* Create a new address space. */
        as = as_create();
        if (as == NULL) {
                vfs_close(v);
		*err= ENOMEM;
		lock_release(execbuf_lock);
                return 0;
        }

	as_destroy(curproc->p_addrspace);
        /* Switch to it and activate it. */
        proc_setas(as);
        as_activate();
	
        /* Load the executable. */
        result = load_elf(v, &entrypoint);
        if (result) {
                /* p_addrspace will go away when curproc is destroyed */
                vfs_close(v);
		*err = result;
		lock_release(execbuf_lock);
                return 0;
        }

        /* Done with the file now. */
        vfs_close(v);

        /* Define the user stack in the address space */
        result = as_define_stack(as, &stackptr);
	if (result) {
                /* p_addrspace will go away when curproc is destroyed */
                 *err = result;
		 lock_release(execbuf_lock);
		 return 0;
        }

	/*As promised above, getting on with loading args into the user stack*/
//	lock_acquire(execbuf_lock);

	/*Initialising the pointer array*/
//	char* arg_pointers[number_args + 1];
	arg_pointers[0] = (char *)(stackptr - execbuf_index);
	size_t prev_index = 0;
	size_t args_counter = 1;
	for(i = 0; i < execbuf_index; i++) {
		if(execbuf[i] == '|') {
			execbuf[i] = '\0';
			size_t execbuf_len = i - prev_index + 1;
			prev_index = i+1;
			arg_pointers[args_counter] = (char *)(arg_pointers[args_counter-1] + execbuf_len);
			args_counter++;
		}
	}
	arg_pointers[args_counter] = NULL;

	/*Copying the pointer array out to the user stack*/
	*err = copyout((void *)arg_pointers, (userptr_t)(arg_pointers[0] - (4 * number_args + 4)), 4 * number_args + 4);
	if(*err != 0)
	{
		lock_release(execbuf_lock);
		return 0;
	}
	
	/*Copying the buffer out to the user stack*/
	char* execbuf_start = (char *)(stackptr - execbuf_index);
	*err = copyout(&execbuf, (userptr_t)(execbuf_start), execbuf_index);
        if(*err != 0)
	{
		lock_release(execbuf_lock);
                return 0;
	}

	/*Decrementing the stack pointer to point to the start of the pointer array*/
	stackptr = stackptr - execbuf_index - (4 * number_args + 4);
	lock_release(execbuf_lock);
	/* Wrap to user mode. */
        enter_new_process(number_args /*argc*/, (userptr_t)(arg_pointers[0] - (4 * number_args + 4)) /*userspace addr of argv*/,
                          NULL /*userspace addr of environment*/,
                          stackptr, entrypoint);

        /* enter_new_process does not return. */
        panic("enter_new_process returned\n");
        return EINVAL;
}
