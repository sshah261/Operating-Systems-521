#include <kern/unistd.h>

ssize_t read(int fd, const void *buf, size_t buflen, int *flag);

ssize_t write(int fd, const void *buf, size_t buflen, int *flag);

int open(const char *filename, int flags, int *err);

int close(int fd, int *flag);

off_t lseek(int fd, off_t pos, int whence, int *err);

int chdir (const char *);

int dup2(int, int);
