#include <synch.h>

extern char execbuf[65536];
extern struct lock *execbuf_lock;
extern size_t execbuf_index;
extern size_t number_args;
extern char* arg_pointers[3852];
