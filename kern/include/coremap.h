#include <spinlock.h>
#include <synch.h>
#include <addrspace.h>
#include <cpu.h>

#define FIXED 0 /* Fized page, not to be swapped */
#define FREE 1 /* Free pages, yet to be assigned */
#define USED 2 /* Allocated, and available for swapping */

struct page_stat {
        int state;
        unsigned long chunk_size;
        paddr_t physical_addr;
        vaddr_t virtual_addr;
	struct lock *page_lock;
	struct addrspace *proc_as;
	struct cpu *proc_cpu;
	int lru;
};

extern struct page_stat *coremap;
extern struct page_stat *coremap_freepage;
extern unsigned long noKernelPages;
extern unsigned long totalPages;
extern unsigned long noCoremapPages;
extern unsigned long noOfUsedPages;
extern struct spinlock coremap_spinlock;
extern struct page_stat *rr_coremap;
extern int swap_enabled;
