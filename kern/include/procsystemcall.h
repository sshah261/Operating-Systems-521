#include <types.h>

void exit(int);

void print_buf(size_t);

int donothing(void);

pid_t waitpid(pid_t, int *, int, int *);

pid_t getpid(void);

int execv(char *, char **, int *);
