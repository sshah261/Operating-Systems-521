/*
 * Copyright (c) 2001, 2002, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Driver code is in kern/tests/synchprobs.c We will
 * replace that file. This file is yours to modify as you see fit.
 *
 * You should implement your solution to the whalemating problem below.
 */

#include <types.h>
#include <lib.h>
#include <thread.h>
#include <test.h>
#include <synch.h>

/*
 * Called by the driver during initialization.
 */

struct cv *cv_male;
struct cv *cv_female;
struct cv *cv_matcher;
struct lock *lock_male;
struct lock *lock_female;
struct lock *lock_matcher;
volatile int male_count = 0;
volatile int female_count = 0;
volatile int matcher_count = 0;
	
void 
whalemating_init() {
	cv_male = cv_create("Male"); 	
	cv_female = cv_create("Female");
	cv_matcher = cv_create("Matcher");
	lock_male = lock_create("lock_male");
	lock_female = lock_create("lock_female");
	lock_matcher = lock_create("lock_matcher");
	return;
}

/*
 * Called by the driver during teardown.
 */
void
whalemating_cleanup() {
	cv_destroy(cv_male);
	cv_destroy(cv_female);
	cv_destroy(cv_matcher);
	lock_destroy(lock_male);
	lock_destroy(lock_female);  
	lock_destroy(lock_matcher);  
	return;
}

void
male(uint32_t index)
{
	male_start(index);
	lock_acquire(lock_male);
	male_count++;
	if(female_count == 0 || matcher_count == 0)
	 	cv_wait(cv_male,lock_male);
	else
	{
		cv_signal(cv_female,lock_male);
		cv_signal(cv_matcher,lock_male);
	}
	male_count--;
	lock_release(lock_male);
	male_end(index);
	return;
}

void
female(uint32_t index)
{
	female_start(index);
	lock_acquire(lock_female);
	female_count++;
	if(male_count == 0 || matcher_count == 0)
		cv_wait(cv_female,lock_female);
	else
	{
		cv_signal(cv_male,lock_female);
		cv_signal(cv_matcher,lock_female);
	}
	female_count--;
	lock_release(lock_female);
	female_end(index);
	return;
}

void
matchmaker(uint32_t index)
{
	matchmaker_start(index);
	lock_acquire(lock_matcher);
	matcher_count++;
	if(male_count == 0 || female_count == 0)
		cv_wait(cv_matcher,lock_matcher);
	else
	{
		cv_signal(cv_male,lock_matcher);
		cv_signal(cv_female,lock_matcher);
	}
	matcher_count--;
	lock_release(lock_matcher);
	matchmaker_end(index);
	return;
}
