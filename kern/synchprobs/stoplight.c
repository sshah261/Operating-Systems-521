/*
 * Copyright (c) 2001, 2002, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Driver code is in kern/tests/synchprobs.c We will replace that file. This
 * file is yours to modify as you see fit.
 *
 * You should implement your solution to the stoplight problem below. The
 * quadrant and direction mappings for reference: (although the problem is, of
 * course, stable under rotation)
 *
 *   |0 |
 * -     --
 *    01  1
 * 3  32
 * --    --
 *   | 2|
 *
 * As way to think about it, assuming cars drive on the right: a car entering
 * the intersection from direction X will enter intersection quadrant X first.
 * The semantics of the problem are that once a car enters any quadrant it has
 * to be somewhere in the intersection until it call leaveIntersection(),
 * which it should call while in the final quadrant.
 *
 * As an example, let's say a car approaches the intersection and needs to
 * pass through quadrants 0, 3 and 2. Once you call inQuadrant(0), the car is
 * considered in quadrant 0 until you call inQuadrant(3). After you call
 * inQuadrant(2), the car is considered in quadrant 2 until you call
 * leaveIntersection().
 *
 * You will probably want to write some helper functions to assist with the
 * mappings. Modular arithmetic can help, e.g. a car passing straight through
 * the intersection entering from direction X will leave to direction (X + 2)
 * % 4 and pass through quadrants X and (X + 3) % 4.  Boo-yah.
 *
 * Your solutions below should call the inQuadrant() and leaveIntersection()
 * functions in synchprobs.c to record their progress.
 */

#include <types.h>
#include <lib.h>
#include <thread.h>
#include <test.h>
#include <synch.h>

/*
 * Called by the driver during initialization.
 */

struct lock *lock_zero;
struct lock *lock_one;
struct lock *lock_two;
struct lock *lock_three;

void acquire(uint32_t);
void release(uint32_t);
void swap(uint32_t *, uint32_t *);

void
stoplight_init() {
	lock_zero = lock_create("lock_zero");
	lock_one = lock_create("lock_one");
	lock_two = lock_create("lock_two");
	lock_three = lock_create("lock_three");
	return;
}

/*
 * Called by the driver during teardown.
 */

void stoplight_cleanup() {
	lock_destroy(lock_zero);
	lock_destroy(lock_one);
	lock_destroy(lock_two);
	lock_destroy(lock_three);
	return;
}

void
turnright(uint32_t direction, uint32_t index)
{
	//(void)direction;
	//(void)index;
	acquire(direction);
	inQuadrant(direction, index);
	leaveIntersection(index);
	release(direction);
	return;
}
void
gostraight(uint32_t direction, uint32_t index)
{
	uint32_t newDirection = (direction + 3) % 4;
	uint32_t dir0, dir1;
	if(direction < newDirection) {
		dir0 = direction;
		dir1 = newDirection;
	}
	else {
		dir0 = newDirection;
		dir1 = direction;
	}
	acquire(dir0);
	acquire(dir1);
	inQuadrant(direction, index);
	inQuadrant(newDirection, index);
	leaveIntersection(index);
	release(dir0);
	release(dir1);
	return;
}
void
turnleft(uint32_t direction, uint32_t index)
{
	uint32_t intermediateDirection = (direction + 3) % 4;
	uint32_t finalDirection = (direction + 2) % 4;
	uint32_t dir0, dir1, dir2;
	dir0 = direction;
	dir1 = intermediateDirection;
	dir2 = finalDirection;
	//kprintf("Old: %d\t%d\t%d\n", dir0, dir1, dir2);
	if(dir0 > dir2)
		swap(&dir0, &dir2);
	if(dir0 > dir1)
		swap(&dir0, &dir1);
	if(dir1 > dir2)
		swap(&dir1, &dir2);
	//kprintf("New: %d\t%d\t%d\n", dir0, dir1, dir2);
	acquire(dir0);
	acquire(dir1);
	acquire(dir2);
	inQuadrant(direction, index);
	inQuadrant(intermediateDirection, index);
	inQuadrant(finalDirection, index);
	leaveIntersection(index);
	release(dir0);
	release(dir1);
	release(dir2);
	return;
}

void
swap(uint32_t *a, uint32_t *b)
{
	uint32_t x = *a;
	*a = *b;
	*b = x;
	return;
}

void
acquire(uint32_t direction)
{
	switch(direction)
	{
		case 0: lock_acquire(lock_zero);
			break;

		case 1: lock_acquire(lock_one);
			break;
		
		case 2: lock_acquire(lock_two);
			break;

		case 3: lock_acquire(lock_three);
			break;
	}	
}

void
release(uint32_t direction)
{
	switch(direction)
	{
		case 0: lock_release(lock_zero);
			break;
		case 1: lock_release(lock_one);
			break;
		case 2: lock_release(lock_two);
			break;
		case 3: lock_release(lock_three);
			break;
	}
}
